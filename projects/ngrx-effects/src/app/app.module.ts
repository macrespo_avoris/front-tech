import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { errorReducer } from './store/reducers/error.reducer';
import { booksReducer } from './store/reducers/books.reducer';
import { collectionReducer } from './store/reducers/collection.reducer';
import { StoreModule } from '@ngrx/store';

import { BooksModule } from "./containers/books/books.module";

import { EffectsModule } from '@ngrx/effects';
import { BooksEffect } from "./store/effects/books.effect";

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BooksModule,
    StoreModule.forRoot({ error: errorReducer, books: booksReducer, collection: collectionReducer }),
    EffectsModule.forRoot([BooksEffect])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
