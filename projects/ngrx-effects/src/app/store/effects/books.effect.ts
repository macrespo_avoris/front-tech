import { Injectable } from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {of } from 'rxjs';
import { map, mergeMap, catchError } from 'rxjs/operators';
import {GoogleBooksService} from "../../containers/books/books.service";

@Injectable()
export class BooksEffect {

  loadBooks$ = createEffect(() => this.actions$.pipe(
      ofType('[Book List] Retrieve Books'),
      mergeMap(() => this.booksService.getBooks()
        .pipe(
          map(books => ({ type: '[Book List/API] Retrieve Books Success', books: books })),
          // mock to reproduce an error ... comment next line to delete the error
          map(books => { throw new Error('Hello') }),
          catchError(() => of({ type: '[Books API] Books Loaded Error' }))
        ))
    )
  );

  constructor(
    private actions$: Actions,
    private booksService: GoogleBooksService
  ) {}
}
