import { createReducer, on } from '@ngrx/store';
import { addBook, removeBook } from '../actions/books.actions';

export const initialState: ReadonlyArray<string> = [];

export const collectionReducer = createReducer(
  initialState,
  on(removeBook, (state, { bookId }) => {
    console.log('removing book ' + bookId + ' from state');

    return state.filter((id) => id !== bookId);
  }),
  on(addBook, (state, { bookId }) => {
    if (state.indexOf(bookId) > -1) {
      console.log('since book ' + bookId + ' is already in the collection, it does noting');
      return state;
    }
    console.log('adding book ' + bookId + ' to state');
    return [...state, bookId];
  })
);
