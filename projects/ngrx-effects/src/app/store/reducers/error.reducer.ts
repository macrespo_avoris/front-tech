import { createReducer, on } from '@ngrx/store';

import { doError } from '../actions/books.actions';

export const initialState: boolean = false;

export const errorReducer = createReducer(
  initialState,
  on(doError, (state) => {
      console.log('assigning true to error')

      return true;
  })
);
