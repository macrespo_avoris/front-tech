import { createSelector, createFeatureSelector } from '@ngrx/store';
import { Book } from '../../containers/books/books.model';
import {AppState} from "../app.state";

const selectBooks = (state: AppState): readonly Book[] => state.books;

export const selectCollectionState = createFeatureSelector<ReadonlyArray<string>>('collection');

export const selectError = createFeatureSelector<boolean>('error');

export const selectBookCollection = createSelector(
  selectBooks,
  selectCollectionState,
  (books, collection) => {
    return collection.map((id) => books.find((book) => book.id === id));
  }
);
