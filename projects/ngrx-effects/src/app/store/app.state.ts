import { Book } from '../containers/books/books.model';

export interface AppState {
  error: boolean;
  books: ReadonlyArray<Book>;
  collection: ReadonlyArray<string>;
}
