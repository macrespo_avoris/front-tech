import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from "rxjs";

import {selectBookCollection, selectError} from './store/selectors/books.selectors';
import {
  addBook,
  removeBook,
} from './store/actions/books.actions';
import { GoogleBooksService } from './containers/books/books.service';
import { Book } from './containers/books/books.model';
import { AppState } from "./store/app.state";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent {
  error$ = this.store.select(selectError);
  books$: Observable<readonly Book[]> = this.store.select((state: AppState) => state.books);
  bookCollection$ = this.store.select(selectBookCollection);

  onAdd(bookId: string) {
    console.log('onAdd -> dispatching \'addBook\' ' + bookId);

    this.store.dispatch(addBook({ bookId }));
  }

  onRemove(bookId: string) {
    console.log('onRemove -> dispatching \'removeBook\' ' + bookId);

    this.store.dispatch(removeBook({ bookId }));
  }

  constructor(
    private booksService: GoogleBooksService,
    private store: Store
  ) {}

  ngOnInit() {
    console.log('call to booking service to give us a few books');

    this.store.dispatch({ type: '[Book List] Retrieve Books'});
  }
}
