import { createReducer, on } from '@ngrx/store';

import { retrievedBookList } from '../actions/books.actions';
import { Book } from '../../containers/books/books.model';

export const initialState: ReadonlyArray<Book> = [];

export const booksReducer = createReducer(
  initialState,
  on(retrievedBookList, (state, { books }) => {
      console.log('assigning values to books in state')

      return books;
  })
);
