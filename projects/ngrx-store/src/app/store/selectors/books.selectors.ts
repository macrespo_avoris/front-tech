import { createSelector, createFeatureSelector } from '@ngrx/store';
import { Book } from '../../containers/books/books.model';
import {AppState} from "../app.state";

export const selectBooks = (state: AppState): ReadonlyArray<Book> => state.books;

export const selectCollectionState = createFeatureSelector<ReadonlyArray<string>>('collection');

export const selectBookCollection = createSelector(
  selectBooks,
  selectCollectionState,
  (books, collection) => {
    return collection.map((id) => books.find((book) => book.id === id));
  }
);
