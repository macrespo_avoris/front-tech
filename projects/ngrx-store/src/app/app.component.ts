import { Component } from '@angular/core';
import { Store } from '@ngrx/store';

import { selectBookCollection, selectBooks } from './store/selectors/books.selectors';
import {
  retrievedBookList,
  addBook,
  removeBook,
} from './store/actions/books.actions';
import { GoogleBooksService } from './containers/books/books.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent {
  books$ = this.store.select(selectBooks);
  bookCollection$ = this.store.select(selectBookCollection);

  onAdd(bookId: string) {
    console.log('onAdd -> dispatching \'addBook\' ' + bookId);

    this.store.dispatch(addBook({ bookId }));
  }

  onRemove(bookId: string) {
    console.log('onRemove -> dispatching \'removeBook\' ' + bookId);

    this.store.dispatch(removeBook({ bookId }));
  }

  constructor(
    private booksService: GoogleBooksService,
    private store: Store
  ) {}

  ngOnInit() {
    console.log('call to booking service to give us a few books');

    this.booksService
      .getBooks()
      .subscribe((books) => {
        console.log('booking service has notified that returned a few books');

        this.store.dispatch(retrievedBookList({ books }));
      });
  }
}
