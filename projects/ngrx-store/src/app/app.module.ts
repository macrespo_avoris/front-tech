import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { booksReducer } from './store/reducers/books.reducer';
import { collectionReducer } from './store/reducers/collection.reducer';
import { StoreModule } from '@ngrx/store';

import { BooksModule } from "./containers/books/books.module";

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BooksModule,
    StoreModule.forRoot({ books: booksReducer, collection: collectionReducer })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
